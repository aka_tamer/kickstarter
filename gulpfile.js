const { task, watch, series } = require('gulp');
const
	gulp 						= require('gulp'),
	data 						= require('gulp-data'),
	browserSync 		= require('browser-sync').create(),
	sass        		= require('gulp-sass'),
	nunjucksRender 	= require('gulp-nunjucks-render');

gulp.task('njk', function() {
	// Gets .html and .nunjucks files in pages
	return gulp.src('app/pages/**/*.+(njk)')
	// Adding data to Nunjucks
	.pipe(data(function() {
      return require('./app/data.json')
    }))
	// Renders template with nunjucks
	.pipe(nunjucksRender({
		 path: ['app/templates']
	  }))
	// output files in app folder
	.pipe(gulp.dest('app'))
});

gulp.task('bs', function() {
	browserSync.init({
			server: {
					baseDir: "./"
			}
	});
});

gulp.task('sass', function() {
	return gulp.src("app/scss/*.scss")
			.pipe(sass())
			.pipe(gulp.dest("app/css"))
			.pipe(browserSync.stream());
});

function browsersyncReload(cb){
  browsersync.reload();
  cb();
}

// Watch Task
task('watchTask', function () {
  watch('*.html', browsersyncReload);
  watch(['app/**/*.scss'], series(sass, bs));

});

// Task: Default task - build assets, pages and docs for dev purposes
task('dev',
  series(
    'njk',
    'bs',
    'sass',
    'watchTask' 
  )
);
